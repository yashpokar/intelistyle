from api import create_app, settings

app = create_app(settings)

if __name__ == '__main__':
    app.run()
