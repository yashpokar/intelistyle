from celery import Celery
import time
import uuid

from flask import Flask, g
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mongoengine import MongoEngine
from flask_cors import CORS

limiter = Limiter(key_func=get_remote_address,
                  default_limits=['60 per minute'])
db = MongoEngine()
cors = CORS()


def create_app(config):
    """
    Factory to create a fresh
    application instance

    :param config:
    :return:
    """

    app = Flask(__name__)
    app.config.from_object(config)

    db.init_app(app)
    limiter.init_app(app)
    cors.init_app(app, resource={'*': {
        'origins': app.config['ALLOWED_ORIGINS'],
        'Access-Control-Allow-Origin': app.config['ALLOWED_ORIGINS'],
    }})

    from .error.handlers import handlers
    from .listing.views import listing
    from .main.views import main

    app.register_blueprint(handlers)
    app.register_blueprint(main)
    app.register_blueprint(listing)

    @app.before_request
    def intercept_request():
        """

        :return:
        """
        g.request_id = uuid.uuid4()
        g.arrived_at = time.time()

        app.logger.info('request %s arrived', g.request_id)

    @app.after_request
    def intercept_response(response):
        """

        :param response:
        :return:
        """
        execution_time = time.time() - g.arrived_at
        app.logger.info('request %s took %s seconds for the execution', g.request_id, execution_time)

        return response

    return app


def make_celery(app=None):
    """

    :param app:
    :return:
    """

    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            """

            :param args:
            :param kwargs:
            :return:
            """

            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery
