from flask import Blueprint, jsonify, current_app as app
from mongoengine.errors import OperationError

handlers = Blueprint('handlers', __name__)


@handlers.app_errorhandler(Exception)
def handle_global_error(error):
    """

    :param error:
    :return:
    """
    message = 'Internal server error.'
    app.logger.error('unknown error %s', error)

    if app.config['DEBUG']:
        message = str(error)

    return jsonify(success=False, message=message), 500


@handlers.app_errorhandler(404)
def handle_resource_not_found_error(error):
    """

    :param error:
    :return:
    """
    app.logger.error('resource not found %s', error)

    return jsonify(success=False,
                   message='Resource not found.'), 404


@handlers.app_errorhandler(OperationError)
def handle_database_error(error):
    """
    TODO : handle this inside general exception
        or continue throwing an exception from here
        to follow "DRY"

    :param error:
    :return:
    """

    if app.config['DEBUG']:
        app.logger.error('database error found %s', error)

    return jsonify(success=False,
                   message='Internal server error.'), 500
