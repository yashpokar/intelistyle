from flask import Blueprint, jsonify

from .models import Task, TaskType

main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
def index():
    return jsonify(success=True, message='Welcome to Garment\'s API')


@main.route('/stats', methods=['GET'])
def stats():
    task = Task.objects(type=TaskType.PULL_PRODUCTS)

    if not task:
        return jsonify(success=True, message='Product listing is not yet started dumping.', flag='ERROR')

    if task.started_at and not task.completed_at:
        return jsonify(success=True, message='Products are being pulled to our system.', flag='PROGRESS')

    return jsonify(success=True,
                   message='All the products are retrieved successfully.', flag='SUCCESS')
