from .. import db

from enum import Enum


class TaskType(Enum):
    PULL_PRODUCTS = 'PULL_PRODUCTS'


class Task(db.Document):
    type = db.StringField()
    started_at = db.DateTimeField()
    completed_at = db.DateTimeField()

    meta = dict(
        indexes=[
            '#type',
        ]
    )
