from .. import db


class Image(db.EmbeddedDocument):
    url = db.StringField()
    path = db.StringField()
    checksum = db.StringField()


class Product(db.Document):
    product_id = db.IntField()
    url = db.StringField()
    gender = db.StringField()
    price = db.DecimalField()
    description = db.StringField()
    source = db.StringField()
    title = db.StringField()
    brand = db.StringField()
    currency_code = db.StringField()
    stock = db.IntField()
    categories_mapped = db.ListField(db.IntField())
    images = db.ListField(db.EmbeddedDocumentField(Image))
    categories = db.ListField(db.StringField())
    position = db.ListField(db.StringField())
    image_urls = db.ListField(db.StringField())
    images_alternative = db.ListField(db.StringField())

    meta = dict(
        indexes=[
            dict(
                fields=['$title', '$description', '$brand'],
                default_language='english',
                weights=dict(
                    title=10,
                    description=8,
                    brand=5,
                )
            )
        ],
    )
