from flask_wtf import FlaskForm
from wtforms.fields import StringField
from wtforms.validators import DataRequired, Length


class SearchListingForm(FlaskForm):
    query = StringField(validators=[
        DataRequired(message='Query is required'),
        Length(min=3, message='At least three characters are required to search garments'),
    ])
