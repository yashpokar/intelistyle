from flask import Blueprint, jsonify, request
from markupsafe import escape

from .models import Product
from .forms import SearchListingForm

listing = Blueprint('listing', __name__)


@listing.route('/search', methods=['GET'])
def search():
    """

    :return:
    """
    form = SearchListingForm(request.args)

    if not form.validate():
        return jsonify(success=False,
                       message='Validation failed.', errors=form.errors), 422

    query = escape(form.data['query'])

    products = Product.objects.search_text(query).order_by('$text_score')[:10]

    return jsonify(success=True,
                   message='Listed all the matches to your query.', products=products)
