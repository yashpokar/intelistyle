from ..utils.parser import (
    Parser,
    StringField,
    IntegerField,
    FloatField,
    ListField
)


class ImageParser(Parser):
    url = StringField()
    path = StringField()
    checksum = StringField()


class ProductParser(Parser):
    product_id = IntegerField()
    title = StringField('product_title')
    url = StringField()
    gender = StringField()
    price = FloatField()
    description = StringField('product_description')
    source = StringField()
    categories = ListField('product_categories', parser=StringField())
    categories_mapped = ListField('product_categories_mapped', parser=IntegerField())
    images = ListField(parser=ImageParser)
    # TODO :: this seems be having same values as above
    # also images contains more details including these two
    # but that's fine too!
    image_urls = ListField(parser=StringField())
    images_alternative = ListField('product_imgs_src', parser=StringField())
    position = ListField(parser=StringField())
    # TODO :: this is same as source, but that's fine!
    brand = StringField()
    currency_code = StringField()
    stock = IntegerField()
