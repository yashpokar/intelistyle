from . import create_app, make_celery
from . import settings
from .listing.mapper import ProductParser
from .listing.models import Product

import json
from celery.signals import worker_ready

from .utils.services import GarmentProductsService

app = create_app(settings)
celery = make_celery(app)


@worker_ready.connect
def on_worker_is_ready(sender=None, headers=None, body=None, **kwargs):
    """

    :param sender:
    :param headers:
    :param body:
    :param kwargs:
    :return:
    """
    pull_data.delay()


@celery.task
def pull_data():
    """

    :return:
    """

    try:
        service = GarmentProductsService(url=app.config['GARMENTS_DATA_SOURCE'])
        products = service.fetch()

        for product in products:
            try:
                app.logger.info('-----------------------')
                item = json.loads(product)

                parser = ProductParser(item)

                item = parser.parse()
                app.logger.info('parsed data %s', item)

                product = Product(
                    product_id=item.product_id,
                    title=item.title,
                    url=item.url,
                    gender=item.gender,
                    price=item.price,
                    description=item.description,
                    source=item.source,
                    categories=item.categories,
                    categories_mapped=item.categories_mapped,
                    images=item.images,
                    image_urls=item.image_urls,
                    images_alternative=item.images_alternative,
                    position=item.position,
                    brand=item.brand,
                    currency_code=item.currency_code,
                    stock=item.stock,
                )
                Product.objects.insert(product)

            except ValueError as e:
                app.logger.error('json decoding failed %s, when encountered %s', e, product)
    except TimeoutError as error:
        app.logger.error('could not pull the data because of request timeout %s', error)
