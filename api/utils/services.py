import requests


class GarmentProductsService(object):
    def __init__(self, url, chunk_size=1024):
        self.url = url
        self.chunk_size = chunk_size

    def fetch(self):
        r = requests.get(self.url, stream=True)

        for line in r.iter_lines(chunk_size=self.chunk_size):
            if line:
                yield line.decode('utf-8')
