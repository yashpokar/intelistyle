from abc import ABC

__all__ = ('StringField', 'Parser',
           'IntegerField', 'FloatField', 'BooleanField', 'ListField')


class Attribute(object):
    def __init__(self, name, data):
        self._name = name
        self._data = data

    @property
    def data(self):
        return self._data

    @property
    def name(self):
        return self._name


class Field(ABC):
    def __init__(self, name=None):
        self.name = name
        self.data = None

    def parse_value(self, attr: Attribute):
        name = self.name

        if name is None:
            name = attr.name

        return attr.data[name]


class ParsedData(object):
    def __init__(self, data):
        self.data = data

    def __getattr__(self, item):
        if item not in self.data:
            raise KeyError(f'{item} not exists.')
        return self.data[item]

    def __getitem__(self, item):
        return self.__getattr__(item)

    def __str__(self):
        string = ''

        for key, value in self.parsed_data.items():
            string += f'\t{key}: {value}\n'

        return string


class Parser(ABC):
    def __init__(self, data):
        self.data = data
        self.parsed_data = dict()

    def parse(self) -> ParsedData:
        """

        :return: ParsedData
        """
        attrs = dir(self)

        # TODO : this can be threaded,
        #  since these are an independent tasks
        for attr_name in attrs:
            attr = getattr(self, attr_name)

            if not isinstance(attr, Field):
                continue

            attribute = Attribute(attr_name, self.data)
            self.parsed_data[attr_name] = attr.parse_value(attribute)

        return ParsedData(self.parsed_data)


class StringField(Field):
    def extract_value(self, attr: Attribute):
        return super().parse_value(attr).trim()


class FloatField(Field):
    def extract_value(self, attr: Attribute):
        return float(super().parse_value(attr).trim())


class IntegerField(Field):
    def extract_value(self, attr: Attribute):
        return int(super().parse_value(attr).trim())


class BooleanField(Field):
    def extract_value(self, attr: Attribute):
        value = super().parse_value(attr)

        if isinstance(value, bool):
            return value

        return bool(value.trim())


class ListField(Field):
    def __init__(self, name=None, parser=None, default=None):
        super().__init__(name)

        if default is None:
            default = []

        self.parser = parser
        self.default = default

    def extract_value(self, attr: Attribute):
        items = super().parse_value(attr)

        if not items:
            return self.default

        if isinstance(self.parser, Field):
            return [self.parser.parse_value(Attribute(name=idx, data=items))
                    for idx, item in enumerate(items)]

        return [self.parser(Attribute(name=idx, data=item)).parse()
                for idx, item in enumerate(items)]
