import os

from dotenv import load_dotenv

load_dotenv()

DEBUG = os.getenv('DEBUG', 'False').title() == 'True'
TESTING = os.getenv('TESTING', 'False').title() == 'True'
ENV = os.getenv('ENV', 'production')
SECRET_KEY = os.getenv('SECRET_KEY', b'THE-SECRET-KEY')

MONGODB_DB = 'garment'
MONGODB_HOST = os.getenv('MONGODB_HOST', '127.0.0.1')
MONGODB_PORT = int(os.getenv('MONGODB_PORT', 27017))
MONGODB_USERNAME = os.getenv('MONGODB_USERNAME', 'root')
MONGODB_PASSWORD = os.getenv('MONGODB_PASSWORD', 'root')

# TODO : Make it specific to our frontend
ALLOWED_ORIGINS = '*'

WTF_CSRF_ENABLED = False

GARMENTS_DATA_SOURCE = os.getenv('GARMENTS_DATA_SOURCE')

if not GARMENTS_DATA_SOURCE:
    raise ValueError('"GARMENTS_DATA_SOURCE" does not exists.')

CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')

if not CELERY_BROKER_URL:
    raise ValueError('"CELERY_BROKER_URL" does not exists.')
