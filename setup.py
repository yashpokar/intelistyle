import sys
from setuptools import setup, find_packages

REQUIRED_PYTHON = (3, 6)
CURRENT_PYTHON = sys.version_info[:2]

if CURRENT_PYTHON < REQUIRED_PYTHON:
    sys.stderr.write("""
=====================================
Unsupported Python version
=====================================

This version of garment api requires Python {}.{}, but you're trying to
install it on Python {}.{}.
""".format(*REQUIRED_PYTHON, *CURRENT_PYTHON))
    sys.exit(1)


setup(
    name='api',
    version='0.1',
    description='The garment API',
    author='Yash Patel',
    author_email='hello@yashpokar.com',
    packages=find_packages(exclude=('tests/',)),
    install_requires=(
        'Flask',
        'Flask-Limiter[redis]',
        'Flask-WTF',
        'flask-cors',
        'flask-mongoengine',
        'celery',
        'python-dotenv',
        'redis',
        'pymongo[srv]',
        'requests',
    ),
    test_suite='nose.collector',
    tests_require=['nose'],
    zip_safe=False,
)
