# Garment API
> Flask based garment's REST API

## Installation Guide
- Install [Mongodb](https://docs.mongodb.com/manual/installation/) based on operating system you are using
- Make sure you have python3.6+ installed in your system

```git clone <repository-url>```<br>
```cd api```<br>
```cp .env.example .env```<br>
```pip install -e .```
*Change the values of environment variables in ".env" file* 

## Run Unit Tests
```python setup.py test```
